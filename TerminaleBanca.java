import java.time.scanner;
import java.time.LocalDate;

public class TerminaleBanca {
  public static void main(String[] args) {

Scanner scanner = new Scanner(System.in);

        ContoCorrente cSiric = new ContoCorrente (2500);
        System.out.println("Saldo: " + cSiric.getSaldo());

        System.out.println("Inserire \n  il numero 1. Per prelevare\n o 2. Per depositare");
        int scelta = scanner.nextInt();

        switch(scelta){
            case 1:
                System.out.println("Inserire l'importo da prelevare: ");
                double importo_Prelievo = scanner.nextDouble();
                cSiric.prelievo(importo_Prelievo);
                break;

            case 2:
                System.out.println("Inserisci l'importo da depositare: ");
                double importo_Deposito = scanner.nextDouble();
                cSiric.deposito(importo_Deposito);
                break;

            default:
                System.out.println("Scelta non valida.");
        }
    }
}
