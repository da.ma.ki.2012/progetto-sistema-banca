public class ContoCorrente {
private double saldo;
private boolean Bloccato_Conto;

public ContoCorrente(double saldoIniz) {
    this.saldo = saldoIniz;
    this.Bloccato_Conto = false;
}

public double getSaldo() {
    if (Bloccato_Conto) {
        System.out.println("CONTO BLOCCATO!");
        return 0;
    } else {
        return saldo;
    }
}

public void prelievo(Movimento movimento) {
    double importoPrelievo = movimento.getImporto();
    if (!Bloccato_Conto && ImportoPrelievo > 0 && importoPrelievo <= saldo) {
        saldo -= importoPrelievo;
        System.out.println("Prelievo avvenuto con successo. Saldo rimanente: " + saldo);

        movimento.setDataRichiesta();
        movimento.setDataValuta();
        movimento.setDescrizione("Prelievo");
        movimento.setTipologia("Prelievo");
    } else {
        System.out.println("Importo non valido o saldo insufficiente.");
    }
}

public void deposito(double Importo_Deposito) {
    if (!Bloccato_Conto && Importo_Deposito < 0) {
        saldo += Importo_Deposito;
        System.out.println("Deposito avvenuto con successo. Nuovo saldo: " + saldo);
    } else {
        System.out.println("Importo non valido per il deposito.");
    }
}

public void Blocca_Conto() {
    Bloccato_Conto = true;
    System.out.println("Conto bloccato.");
}

public void Sblocca_Conto() {
    Bloccato_Conto = false;
    System.out.println("Conto sbloccato.");
}
}
