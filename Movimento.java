//Scrivete una nuova classe Movimento che rappresenta un'operazione effettuata sul conto corrente.
//Ogni operazione deve avere:
//data richiesta, data valuta, descrizione (o casuale), tipologia (campo libero può essere bonifico, prelievo, F24, pagamento carta, ecc...)
//(ignoralo[extra])Modificare il codice dei metodi della classe ContoCorrente (bonifico e prelievo in particolare) perché accettino come argomento una variabile di tipo Movimento.
//          ||
//usare:    \/
//import java.time.LocalDate;
//LocalDate date = LocalDate.parse("2018-05-05");
// oppure
//LocalDate date1 = LocalDate.of(2017, 1, 13);

import java.time.LocalDate;


public class Movimento {
  private LocalDate dataRichiesta;
  private LocalDate dataValuta;
  private String descrizione;
  private String tipologia;
  private double importo;

  public Movimento(LocalDate dataRichiesta, LocalDate dataValuta, String descrizione, String tipologia, double importo) {
        this.dataRichiesta = dataRichiesta;
        this.dataValuta = dataValuta;
        this.descrizione = descrizione;
        this.tipologia = tipologia;
        this.importo = importo;
    }

public LocalDate dataRichiesta () {
  return dataRichiesta;
}

public LocalDate dataValuta () {
  return dataValuta;
}

public String descrizione () {
  return descrizione;
}

public String tipologia () {
  return tipologia;
}

public double importo () {
  return importo;
}

public void setDataRichiesta(LocalDate dataRichiesta) {
        this.dataRichiesta = dataRichiesta;
    }

    public void setDataValuta(LocalDate dataValuta) {
        this.dataValuta = dataValuta;
    }

public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

public void setTipologia(String tipologia) {
        this.tipologia = tipologia;
    }

public void setImporto(double importo) {
        this.importo = importo;
    }


}
